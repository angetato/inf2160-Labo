- Tri rapide
qsort :: Ord a => [a] -> [a]
qsort []     = []
qsort (x:[]) = [x]
qsort (x:xs) = qsort (filter (<=x) xs) ++ [x] ++ qsort (filter (>x) xs)

-- Main
main = putStrLn (show (qsort [1,9,8,4,3,7,6,2,5]))
